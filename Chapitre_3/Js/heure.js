var heure = Number(prompt('Entrez le chiffre des heures :'));
var minute = Number(prompt('Entrez le chiffre des minutes :'));
var seconde = Number(prompt('Entrez le chiffre des secondes :'));

if ((heure < 24) && (heure >= 0) && (minute <= 59) && (minute >= 0) && (seconde <= 59) && (seconde >= 0)) {
    if ((heure === 23) && (minute === 59) && (seconde === 59)) {
        console.log('Dans une seconde il sera minuit !');
    }
    else if ((minute === 59) && (seconde === 59)) {
        heure = heure + 1;
        minute = 00;
        seconde = 00;
        console.log('Dans une seconde il sera ' + heure + 'h' + minute + 'm' + seconde + 's');
    }
    else {
        seconde = seconde + 1;
        console.log('Dans une seconde il sera ' + heure + 'h' + minute + 'm' + seconde + 's');
    }
}
else {
    console.log('L\'heure que vous avez donnée est incorrecte, veuillez entrer une heure valide');
}