var mois = Number(prompt('Entrez le numéro du mois que vous voulez :'));

if ((mois < 13) && (mois > 0)) {
    if (mois === 2) {
        console.log('Ce mois compte 28 jours');
    }
    else if ((mois === 1) ||(mois === 3) ||(mois === 5) ||(mois === 7) ||(mois === 8) ||(mois === 10) ||(mois === 12)) {
        console.log('Ce mois compte 31 jours');
    }
    else {
        console.log('Ce mois compte 30 jours');
    }
}
else {
    console.log('Ce numéro de mois n\'existe pas');
}