var jour = String(prompt('Entrez un jour de la semaine :'));
jour = jour.toLowerCase();

switch(jour) {
    case 'lundi':
        console.log('Demain, c\'est Mardi !');
        alert('Demain, c\'est Mardi !');
        break;
    case 'mardi':
        console.log('Demain, c\'est Mercredi !');
        alert('Demain, c\'est Mercredi !');
        break;
    case 'mercredi':
        console.log('Demain, c\'est Jeudi !');
        alert('Demain, c\'est Jeudi !');
        break;
    case 'jeudi':
        console.log('Demain, c\'est Vendredi !');
        alert('Demain, c\'est Vendredi !');
        break;
    case 'vendredi':
        console.log('Demain, c\'est Samedi !');
        alert('Demain, c\'est Samedi !');
        break;
    case 'samedi':
        console.log('Demain, c\'est Dimanche !');
        alert('Demain, c\'est Dimanche !');
        break;
    case 'dimanche':
        console.log('Demain, c\'est Lundi !');
        alert('Demain, c\'est Lundi !');
        break;
    default:
        alert('Le jour saisi est INCORRECT.');
        console.log('Le jour saisi est INCORRECT.');
  }