function compterNbVoyelles(unMot) {
    var voyelles = 0;
    for (i = 0; i < unMot.length; i++) {
        var lettre = unMot[i].toLowerCase();
        if ((lettre === 'a') || (lettre === 'e') || (lettre === 'i') || (lettre === 'o') || (lettre === 'u') || (lettre === 'y')) {
            voyelles++;
        }
    }
    return voyelles;
}

function inverser(unMot) {
    var motInverse = '';
    for (i = 0; i < unMot.length; i++) {
        motInverse = unMot[i] + motInverse;
    }
    return motInverse;
}

function convertirEnLeetSpeak(unMot) {
    var leetSpeak = ''; 
    for (i = 0; i < unMot.length; i++) {
        var lettre = unMot[i].toLowerCase();
        switch (unMot[i]) {
            case 'a':
                leetSpeak = leetSpeak + '4';
                break;
            case 'b':
                leetSpeak = leetSpeak + '8';
                break;
            case 'e':
                leetSpeak = leetSpeak + '3';
                break;
            case 'l':
                leetSpeak = leetSpeak + '1';
                break;
            case 'o':
                leetSpeak = leetSpeak + '0';
                break;
            case 's':
                leetSpeak = leetSpeak + '5';
                break;
            default :
                leetSpeak = leetSpeak + unMot[i];
                break;
        }
    }
    return leetSpeak;
}

var mot = String(prompt('Veuillez saisir un mot :'));

console.log('Le mot ' + mot + ' contient ' + mot.length + ' caractère(s)');
console.log('Il s\'écrit en minuscules ' + mot.toLowerCase());
console.log('Il s\'écrit en majuscules ' + mot.toUpperCase());

var voyelles = compterNbVoyelles(mot);
console.log('Il contient ' + voyelles + ' voyelle(s) et ' + (mot.length - voyelles) + ' consonne(s)');

var motInverse = inverser(mot);
console.log('A l\'envers, ce mot s\'écrit ' + motInverse);

if (motInverse === mot) {
        var palindrome = 'C\'est un palindrome';
    }
    else {
        var palindrome = 'Ce n\'est pas un palindrome';
    }
    console.log(palindrome);

var leetSpeak = convertirEnLeetSpeak(mot);
console.log(leetSpeak);