// Connaitre la longue d'une chaine de caractère
console.log("ABC".length); // Affiche 3

console.log("Je suis une chaîne".length); // Affiche 18

// Mettre une chaine de caractère en maj ou en min
var motInitial = "Bora-Bora";

var motEnMinuscules = motInitial.toLowerCase();

console.log(motEnMinuscules); // Affiche "bora-bora"

var motEnMajuscules = motInitial.toUpperCase();

console.log(motEnMajuscules); // Affiche "BORA-BORA"

// Récupérer un caractère dans une chaine
var sport = "Tennis-ballon"; // 13 caractères

console.log(sport.charAt(0)); // Affiche "T"

console.log(sport[0]); // Affiche "T"

console.log(sport.charAt(6)); // Affiche "-"

console.log(sport[6]); // Affiche "-"

// Parcourir caractère par caractère une chaine
var prenom = "Odile";

for (var i = 0; i < prenom.length; i++) {

    console.log(prenom[i]);

}

