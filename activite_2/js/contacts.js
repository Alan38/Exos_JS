/* Activité : gestion des contacts */

// Creation du prototype
var Contact = {
    // Initialisation
    init: function (nom, prenom) {
        this.nom = nom;
        this.prenom = prenom;
    },
    // Retourne la description du contact
    decrire: function () {
        var description = "Nom: " + this.nom + ", Prénom: " + this.prenom;
        return description;
    }
}

// Creation d'une fonction qui se chargera d'afficher les choix disponibles
function displayChoice () {
    var choice = ["\n1 : Lister les contacts", "2 : Ajouter un contact", "3 : Quitter"];
    
    console.log(choice[0]);
    console.log(choice[1]);
    console.log(choice[2]);
}

// Creation du tableau qui regroupera tout les contacts
var allContacts = [];

// Creation de l'objet contact0, puis on le pousse dans le tableau
var carole = Object.create(Contact);
carole.init("Lévisse", "Carole");
allContacts.push(carole);

// Creation de l'objet contact1, puis on le pousse dans le tableau
var melodie = Object.create(Contact);
melodie.init("Nelsonne", "Mélodie");
allContacts.push(melodie);

// Phrase de bienvenue
console.log("Bienvenue dans votre gestionnaire de contact !");
// Affiche les choix de l'utilisateur
displayChoice();

// Popup qui demande un choix a l'utilisateur
var popupChoice = Number(prompt("Que voulez-vous faire ? (Tapez le chiffre correspondant a votre choix)"));

// Tant que le choix de l'utilisateur est différent de 3
while (popupChoice !== 3) {
    
    // Si le choix = 1
    if (popupChoice === 1) {
        // On affiche la phrase ci dessous
        console.log("\nVoici la liste de vos contacts: ");
        // On parcours ensuite le tableau allContacts pour donner la liste des contacts
        allContacts.forEach(function (contact) {
            console.log(contact.decrire()); 
        });
        // On réaffiche les choix
        displayChoice();
        // On demande un nouveau choix a l'utilisateur
        popupChoice = Number(prompt("Que voulez-vous faire ? (Tapez le chiffre correspondant a votre choix)"));
    }
    else if (popupChoice === 2) { // Si le choix = 2
        var changeNom = String(prompt("Entrez le nom du contact que vous voulez ajouter: ")); // Popup qui demande le nom
        var changePrenom = String(prompt("Entrez maintenant son prénom: ")); // Et le prenom du contact a ajouter
        
        // Si les champs sont vides
        if (changeNom === "" || changePrenom == "") {
            displayChoice();
            // On redemande de faire un choix
            popupChoice = Number(prompt("Vous devez rentrer \nUN NOM ET UN PRENOM\n Que voulez vous faire maintenant ? ;-)"));
        }
        // Si les champs sont remplis avec des nombres
        else if (!isNaN(changeNom) || !isNaN(changePrenom)) {
            displayChoice();
            // On redemande de faire un choix
            popupChoice = Number(prompt("Vous devez rentrer un nom et un prénom\n AVEC DES LETTRES \net PAS DES CHIFFRES!\n Que voulez vous faire maintenant ? ;-)"));
        }
        else { // Sinon
            // On crée un objet newContact sur le prototype Contact
            var newContact = Object.create(Contact);
            // On l'initialise avec ses propriétés
            newContact.init(changeNom, changePrenom);
            // On le pousse a la suite du tableau
            allContacts.push(newContact);
            // On affiche que tout s'est bien passé
            console.log("\nLe contact a bien été ajouté a la liste !\n")

            displayChoice();
            // Et on redemande de faire un choix
            popupChoice = Number(prompt("Que voulez-vous faire ? (Tapez le chiffre correspondant a votre choix)"));
        }
    }
    else { // Si le choix est égal a autre chose que 1, 2 ou 3
        // On redemande de faire un choix
        popupChoice = Number(prompt("Je n'ai pas compris votre choix, veuillez saisir un chiffre en 1 et 3"));
    }
}

// Quand on est sorti de la boucle, cela veut dire qu'on a obligatoirement fait le choix 3, On affiche un message de remerciement.
alert("Merci d'avoir utilisé le gestionnaire de contact ! Si vous voulez recommencer, rechargez simplement la page. A bientôt !");