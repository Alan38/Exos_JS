function min(x, y) {
    var mini = Math.min(x, y);
    return mini;
}

console.log(min(4.5, 5));
console.log(min(19, 9));
console.log(min(1, 1));
