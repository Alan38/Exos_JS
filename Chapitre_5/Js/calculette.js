// Fonction qui calcule nb1 et nb2 selon l'opérateur renseigné plus bas
function calculer(nb1, operateur, nb2) {
    var result; // on crée une variable result
    if (operateur === '+') { // si c'est '+'
        result = nb1 + nb2; // nb1 + nb2
    } else if (operateur === '-') { // si c'est '-'
        result = nb1 - nb2; // nb1 - nb2
    } else if (operateur === '*') { // si c'est '*'
        result = nb1 * nb2; // nb1 * nb2
    } else if (operateur === '/') { // si c'est '/'
        result = nb1 / nb2; // nb1 / nb2
    }
    return result; // retourne le contenu de result
}

console.log(calculer(4, "+", 6));
console.log(calculer(4, "-", 6));
console.log(calculer(2, "*", 0));
console.log(calculer(12, "/", 0));
