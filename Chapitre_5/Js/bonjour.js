// Fonction qui renvoie un message de bienvenue
function direBonjour(prenom, nom) {
    var message = 'Bonjour, ' + prenom + ' ' + nom + ' !';
    return message;
}

// Popup qui demande le prenom
var saisiePrenom = String(prompt('Veuillez saisir votre prénom :'));

//Popup qui demande le nom
var saisieNom = String(prompt('Veuillez saisir maintenant votre nom :'));

// Affiche le résultat de la fonction dans la console
console.log(direBonjour(saisiePrenom, saisieNom));