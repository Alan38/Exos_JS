/* 
Activité : jeu de devinette
*/

// NE PAS MODIFIER OU SUPPRIMER LES LIGNES CI-DESSOUS
// COMPLETEZ LE PROGRAMME UNIQUEMENT APRES LE TODO

console.log("Bienvenue dans ce jeu de devinette !");

// Cette ligne génère aléatoirement un nombre entre 1 et 100
var solution = Math.floor(Math.random() * 100) + 1;

// Décommentez temporairement cette ligne pour mieux vérifier le programme
console.log("(La solution est " + solution + ")");

// TODO : complétez le programme
var nb = Number(prompt('Essayez de deviner le nombre que j\'ai en tête (entre 1 et 100)'));

for (var i = 1; i <= 6; i++) {
    if (nb === solution) {
        console.log('Vous avez trouvé en ' + i + ' essai(s), bravo :-)');
    } else if (i === 6) {
        console.log('Vous n\'aviez que 6 essais, vous avez donc perdu :-(');
    } else if (nb < solution) {
        console.log(nb + ' est trop petit');
    } else if (nb > solution) {
        console.log(nb + ' est trop grand');
    }
    nb = Number(prompt('Ce n\'est pas le nombre auquel je pense, réessaye !'));
}
