// on demande le chiffre a l'user
var nb = Number(prompt('Entrez un nombre :'));

// tant que nb est supérieur a 100 ou inferieur a 50
while ((nb > 100) || (nb < 50)) {
    nb = Number(prompt('Entrez un nombre entre 50 et 100 :')); // on relance le popup
}

// si nb est compris entre 50 et 100
if((nb <= 100) && (nb >= 50)) {
    console.log('Votre chiffre est compris entre 50 et 100'); // on affiche que c'est bon
}

