// exemple boucle while
console.log("Début du programme");

var nombre = 1;

while (nombre <= 5) {

    console.log(nombre);

    nombre++;

}

console.log("Fin du programme");

// exemple boucle for
for (var compteur = 1; compteur <= 5; compteur++) {

    console.log(compteur);

}
