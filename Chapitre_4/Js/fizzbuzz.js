// programme qui liste les nombre de 1 a 100
// Affiche 'Fizz' si le nombre est divisible par 3
// Affiche 'Buzz' si le nombre est divisible par 5
// Affiche 'FizzBuzz' si le nombre est divisible a la fois par 3 et par 5

// tant que i<100, on incrémente de 1
for (var i = 1; i <= 100; i++) {
    if((i%3===0)&& (i%5===0)) { // si i est divisible par 3 et par 5
        console.log('"Fizzbuzz"'); // affiche 'fizzbuzz'
    }
    else if (i % 5 === 0) { // si il est divisible par 5
        console.log('"Buzz"'); // affiche 'buzz'
    } 
    else if (i % 3 === 0) { // si il est divisible par 3
        console.log('"Fizz"'); // affiche 'fizz'
    } 
    else { //sinon
        console.log(i); // affiche i (donc le nombre)
    }
}
