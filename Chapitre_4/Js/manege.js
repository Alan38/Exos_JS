// programme qui demande un nombre de tour a l'user
// et lui faire faire le nombre de tour souhaité

// on demande le nombre de tour
var tours = Number(prompt('Entrez le nombre de tours que vous souhaitez faire :'));

// on établi un variable tour qui sera égale a 1
var tour = 1;

while (tour <= tours) { // tant que tour est inférieur ou égale a tours
    console.log('C\'est le tour numéro ' + tour); // on affiche le nombre de tour
    tour++; // on incrémente de 1
}
