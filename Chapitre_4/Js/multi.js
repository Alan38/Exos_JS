// on demande le chiffre a l'user
var nb = Number(prompt('Entrez un nombre pour avoir sa table de multiplication :'));

// on crée une variable d'incrémentation
var multi = 1;

// si le chiffre n'est pas compris entre 2 et 9
while ((nb < 2) || (nb > 9)) {
    nb = Number(prompt('Entrez un nombre entre 2 et 9 pour avoir sa table de multiplication :'));
    // alors on redemande un chiffre a l'user
}

console.log('Voici la table de multiplication de ' + nb);
// tant que table de multi est inférieur ou égale a 10
while (multi <= 10) {
    console.log(nb + ' x ' + multi + ' = ' + nb*multi); // on affiche la table
    multi++; // on incrémente le chiffre des multiplication
}