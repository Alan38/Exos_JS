// programme qui compte les chiffres 1 par 1 et nous dit si ils sont pair ou impair

// on demande le chiffre a l'user
var nb = Number(prompt('Entrez un nombre :'));

// on crée une variable d'incrémentation
var i = 1;

while(i <= nb) { // tant que i est inferieur ou égale a nb
    if (i % 2 === 0) { // et si i est pair
        console.log(i + " est pair");
    }
    else { // sinon
        console.log(i + ' est impair');
    }
    i++; // incrémentation de 1
}