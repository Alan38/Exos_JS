// variable qui demande un chiffre en degré
var degre = Number(prompt('Entrez un chiffre (en degré)'));

// variable qui convertit les Celsius en Fahrenheit
var fahrenheit = degre*9/5+32;

// popup qui affiche le resultat
alert(degre + '°C correspond à ' + fahrenheit + '°F');

// on affiche le resultat dans la console
console.log(degre + '°C correspond à ' + fahrenheit + '°F');
