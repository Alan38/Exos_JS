// on créé l'objet prototype
var Film = {
    // initialisation de l'objet
    init: function (titre, annee, realisateur) {
        this.titre = titre;
        this.annee = annee;
        this.realisateur = realisateur;
    },
    // function qui décrit le film
    decrire: function () {
        var description = this.titre + " (" + this.annee + ", " + this.realisateur + ")";
        return description;
    }
};

// on créé le tableau
var films = [];

// creation de l'objet film0 basé sur le prototype Film
var film0 = Object.create(Film);
film0.init("Le loup de Wall Street", 2013, "Martin Scoresse"); // affectation des propriétés du film0
films.push(film0); // on pousse le film0 dans le tableau films

// creation de l'objet film1 basé sur le prototype Film
var film1 = Object.create(Film);
film1.init("Vice-Versa", 2015, "Pete Docter"); // affectation des propriétés du film1
films.push(film1); // on pousse le film1 dans le tableau films

// creation de l'objet film2 basé sur le prototype Film
var film2 = Object.create(Film); 
film2.init("Babysitting", 2013, "Phillippe Lacheau et Nicolas Benamou");// affectation des propriétés du film2
films.push(film2); // on pousse le film2 dans le tableau films

// on parcours le tableau et affiche la description de chaque éléments du tableau grace a la fonction 'decrire'
films.forEach(function (film) {
    console.log(film.decrire());
});
