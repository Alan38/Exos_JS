// déclaration du tableau mots
var mots = [];

// saisie de l'utilisateur
var saisieMot = String(prompt("Veuillez dire quelque chose: "));

// tant que le mot saisie en minuscule est différent dde 'stop'
while(saisieMot.toLowerCase() !== "stop") {
    mots.push(saisieMot); // on pousse le mot saisie dans le tableau mots
    saisieMot = String(prompt("Veuillez dire quelque chose: ")); // on réaffiche la saisie de l'utilisateur
}

// si la boucle est false, donc finie et que l'user a rentrer 'stop'
console.log("Bravo, c'est pas trop tôt ! Voici la liste des mots que vous avez entré avant de me dire 'stop': "); // on affiche cette phrase

// on parcours le tableau puis on affiche les mots qui ont étés poussés dans le tableau
mots.forEach(function (lesMotsSaisis) {
    console.log(lesMotsSaisis); 
});

// OU SINON

// for(var i = 0; i < mots.length; i++) {
//      console.log(mots[i]);      
// }