// stocker des données dans un tableau
var films = ["Le loup de Wall Street", "Vice-Versa", "Babysitting"];

console.log(films.length); // Affiche 3


var tableauVide = []; // Création d'un tableau vide

console.log(tableauVide.length); // Affiche 0


// tableau films
var films = ["Le loup de Wall Street", "Vice-Versa", "Babysitting"];

console.log(films[0]); // Affiche "Le loup de Wall Street"
console.log(films[1]); // Affiche "Vice-Versa"
console.log(films[2]); // Affiche "Babysitting"


// tableau films
var films = ["Le loup de Wall Street", "Vice-Versa", "Babysitting"];

// tant que i < a la longueur du tableau films, on incrémente de 1
for (var i = 0; i < films.length; i++) {
    console.log(films[i]); // affiche le films 1, 2 et ensuite 3
}


// tableau films
var films = ["Le loup de Wall Street", "Vice-Versa", "Babysitting"];

// boucle forEach qui se charge de parcourir le tableau automatiquement
films.forEach(function (film) {
    console.log(film); //  affiche le film
});


// tablleau film
var films = ["Le loup de Wall Street", "Vice-Versa", "Babysitting"];
// on pousse le film dans le tableau (a la suite)
films.push("Les Bronzés");

console.log(films[3]); // Affiche "Les Bronzés"


// ON PEUT A PRESENT STOCKER LES FILMS AVEC LES ANNEES DANS DES OBJETS, PUIS STOCKER LES OBJETS DANS UN TABLEAU
var Film = {
    //Initialise le film
    init: function (titre, annee) {
        this.titre = titre;
        this.annee = annee;
    },
    // function pour décrire le film
    decrire: function () {
        var description = this.titre + " (" + this.annee + ")";
        return description;
    },
};
// creation de l'objet film1 basé sur le prototype Film
var film1 = Object.create(Film);
film1.init("Le loup de Wall Street", 2013);
// idem pour l'objet film2
var film2 = Object.create(Film);
film2.init("Vice-Versa", 2015);
// idem pour l'objet film3
var film3 = Object.create(Film);
film3.init("Babysitting", 2013);

// on crée le tableau films
var films = [];
films.push(film1); // on pousse le film1
films.push(film1); // on pousse le film2
films.push(film1); // on pousse le film3

// function forEach qui permet de parcourir le tableau
films.forEach(function(film) {
    console.log(film.decrire()); // affiche le film grace a la fonction decrire initialisé dans l'objet prototype
});