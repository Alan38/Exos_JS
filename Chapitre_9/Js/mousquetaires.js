// Objet prototype
var Mousquetaire = {
    // on initialise
    init: function(nom) {
        this.nom = nom;
    },
    // function qui se charge de décrire
    decrire: function () {
        var description = this.nom;
        return description;
    },
};

// creation + initialisation de l'objet mousqueton, basé sur le prototype Mousquetaire
var mousqueton = Object.create(Mousquetaire);
mousqueton.init("Athos");

// idem pour mousqueton1
var mousqueton1 = Object.create(Mousquetaire);
mousqueton1.init("Porthos");

// idem
var mousqueton2 = Object.create(Mousquetaire);
mousqueton2.init("Aramis");

// idem
var mousqueton3 = Object.create(Mousquetaire);
mousqueton3.init("D'Artagnan");

// creation d'un tableau vide
var mousquetaires = [];

// on pousse dans le tableau les 3 mousqueton
mousquetaires.push(mousqueton);
mousquetaires.push(mousqueton1);
mousquetaires.push(mousqueton2);

// affiche la phrase ci dessous
console.log("Voici les trois monsquetaires:")
//parcours le tableau et affiche les valeurs dans l'ordre dans lesquelles elles ont étés poussées
mousquetaires.forEach(function (mousqueton) {
   console.log(mousqueton.decrire()); 
});

// on pousse le 4eme mousqueton dans le tableau
mousquetaires.push(mousqueton3);

// affiche la phrase ci dessous
console.log("A présent, ils sont quatre, les voici:");
// on parcours le tableau et on affiche les nouvelles valeurs
mousquetaires.forEach(function (mousqueton) {
    console.log(mousqueton.decrire());
});