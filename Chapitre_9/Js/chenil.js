var Chien = {
    // initialise le chien
    init: function (nom, race, taille) {
        this.nom = nom;
        this.race = race;
        this.taille = taille;
    },
    // Renvoie l'aboiement du chien
    aboyer: function () {
        var aboiement = "Whoua ! Whoua !";
        if (this.taille < 25) {
            aboiement = "Kaii ! Kaii !";
        } 
        else if (this.taille > 60) {
            aboiement = "Grrr ! Grrr !";
        }
        return aboiement;
    },
    // Renvoie la description du chien
    decrire: function() {
        var description = this.nom + " est un " + this.race + " mesurant " + this.taille + "cm. Il aboie: ";
        return description;
    }
};

// Tableau vide nommé 'chiens'
var chiens = [];

// creation de l'objet chien0 basé sur le prototype Chien
var chien0 = Object.create(Chien);
chien0.init("Crokdur" ,"Mâtin de Naples", 75); // Initialisation du chien0 avec ses propriétés
chiens.push(chien0); // On pousse chien0 dans le tableau chiens

// creation de l'objet chien1 basé sur le prototype Chien
var chien1 = Object.create(Chien);
chien1.init("Pupuce", "Bichon", 22); // Initialisation du chien1 avec ses propriétés
chiens.push(chien1); // On pousse chien1 dans le tableau chiens

// creation de l'objet chien2 basé sur le prototype Chien
var chien2 = Object.create(Chien);
chien2.init("Médor", "Labrador", 58); // Initialisation du chien2 avec ses propriétés
chiens.push(chien2); // On pousse chien2 dans le tableau chiens

// Affichage de la phrase ci dessous
console.log("Le chenil héberge 3 chiens: ");

// On parcours le tableau chiens
chiens.forEach(function (chien) {
    console.log(chien.decrire() + chien.aboyer()); // on affiche la description et l'aboiement du chiens grace aux fonction crées précédemment dans le prototype Chien
});

