// Création d'un objet
var stylo = {
    type: "bille",
    couleur: "bleu",
    marque: "Bic"
};

// On peut acceder a l'objet comme ceci :
console.log(stylo.type); // Affiche "bille"
console.log(stylo.couleur); // Affiche "bleu"
console.log(stylo.marque); // Affiche "Bic"

// Autre syntaxe pour accéder aux propriétés de l'objet :
console.log(stylo['type']); // Affiche "bille"
console.log(stylo['couleur']); // Affiche "bleu"
console.log(stylo['marque']); // Affiche "Bic"

// On peut aussi accéder aux propriétés en une seule ligne comme ceci :
console.log('Mon stylo à ' + stylo.type + ' ' + stylo.marque + ' écrit en ' + stylo.couleur);

// Une fois l'objet créé, on peut modifier les valeurs de ses propriétés comme ceci :
stylo.couleur = "rouge"; // Modifie la couleur de l'encre du stylo

console.log("Mon stylo à " + stylo.type + " " + stylo.marque + " écrit en " + stylo.couleur);

// On peut aussi ajouter dynamiquement de nouvelles propriétés a un objet déjà créé :
stylo.prix = 2.5; // ajout de la propriété prix

console.log('Il coûte ' + stylo.prix + ' euros');