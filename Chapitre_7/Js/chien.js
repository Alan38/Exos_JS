// Création de l'objet chien
var chien = {
    nom: 'Crockdur',
    race: 'Mâtin de Naples',
    taille: 75,
    
    // Function qui retourne la description du chien
    decrire: function() {
        var description = this.nom + " est un " + this.race + " mesurant " + this.taille + "cm.";
        return description;
    },
    // Function qui retourne l'aboiement du chien
    aboyer: function() {
        var aboie = "Wouf ! Wouf !";
        return aboie;
    },
}

// Affiche la description du chien
console.log(chien.decrire());
// Affiche l'aboiement du chien
console.log("Tiens, un chat ! " + chien.nom + " aboie: " + chien.aboyer());