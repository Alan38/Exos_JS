// Creation de l'objet
var compte = {
    titulaire: "Alex",
    solde: 0,
    
    // function qui calcule le crédit du compte
    crediter: function(montant) {
        var soldeFinal = this.solde + montant;
        return soldeFinal;
    },
    // function qui calcule le débit du compte
    debiter: function(retrait) {
        var soldeFinal = this.solde - retrait;
        return soldeFinal;
    },
    // function qui affiche la description du compte
    decrire: function() {
        var description = "Titulaire: " + this.titulaire + " | " + "Solde: " + this.solde + " euros.";
        return description;
    }
};

// affichage de la description du compte
console.log(compte.decrire());

// popup qui demande de combien on veut créditer
var credit = Number(prompt("Veuillez créditer ce compte d'une somme de votre choix:"));

// ajout du crédit sur le compte
compte.solde = compte.crediter(credit);

// popup qui demande de combien on veut débiter
var debit = Number(prompt("Veuillez maintenant débiter ce compte du montant de votre choix:"));

// retrait du débit
compte.solde = compte.debiter(debit);

// affichage de la description du compte
console.log(compte.decrire());
