// Popup qui demande le rayon du cercle
var r = Number(prompt("Entrez le rayon d'un cercle:"));

// Création de l'objet cercle
var cercle = {
    // function qui calcule le perimetre
    perimetre: function() {
        var p = Math.PI*(r*2);
        return p;
    },
    // function qui calcule l'aire
    aire: function() {
        var a = Math.PI*(r*r);
        return a;
    },
}


// Affiche le perimetre
console.log('Son périmètre vaut ' + cercle.perimetre());
//Affiche l'aire
console.log('Son aire vaut ' + cercle.aire());