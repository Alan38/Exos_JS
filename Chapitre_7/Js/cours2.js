// Création de l'objet
var perso = {
    nom: "Aurora",
    sante: 150,
    force: 25,
    xp: 0,

    // Renvoie la description du personnage
    decrire: function () {
        var description = this.nom + " a " + this.sante + " points de vie, " +
        this.force + " en force et " + this.xp + " points d'expérience.";
        return description;
    }
};

// Execute la fonction decrire a l'interieur de l'ojet perso et affiche le résultat en console
console.log(perso.decrire());

// Aurora est blessée par une flèche
perso.sante = perso.sante - 20;

// Aurora trouve un bracelet de force
perso.force = perso.force + 10;

// Aurora gagne des points d'expériences
perso.xp = perso.xp + 15;

// Execute la fonction decrire a l'interieur de l'ojet perso et affiche le NOUVEAU résultat en console
console.log(perso.decrire());


/* 
Voici ce que vous devez retenir de ce premier chapitre consacré aux objets :

    La programmation orientée objet consiste à écrire des programmes en utilisant des objets qui représentent les éléments du domaine étudié.

    En JavaScript, un objet est constitué d'un ensemble de propriétés.

    Une propriété est une association entre un nom et une valeur.

    Lorsque la valeur d'une propriété est une fonction, on dit que cette propriété est une méthode de l'objet.
    
    
Voici la syntaxe général de création et d'utilisation d'un objet :

    var monObjet = {
        propriete1: valeur1,
        propriete2: valeur2,
     ... ,
    methode1: function( ... ) {
         ...
    },
    methode2: function( ... ) {
         ...
    },
     ...
};

console.log(monObjet.propriete1); 

console.log(monObjet.methode1(...));
*/