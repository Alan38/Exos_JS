var Personnage = {
    nom: "",
    sante: 0,
    force: 0,
    xp: 0,

    // Renvoie la description du personnage
    decrire: function () {
        var description = this.nom + " a " + this.sante + " points de vie, " +
        this.force + " en force et " + this.xp + " points d'expérience";
        return description;
    }
};

// affectation des propriétés de perso1 en prenant comme prototype Personnage
var perso1 = Object.create(Personnage);
perso1.nom = "Aurora";
perso1.sante = 150;
perso1.force = 25;

// affectation des propriétés de perso2 en prenant comme prototype Personnage
var perso2 = Object.create(Personnage);
perso2.nom = "Glacius";
perso2.sante = 130;
perso2.force = 35;


console.log(perso1.decrire()); // decrire perso1
console.log(perso2.decrire()); // decrire perso2





// AUTRE MANIERE D'AFFECTER DES PROPRIETES EN SE BASANT SUR LE PROTOTYPE PERSONNAGE (PLUS RAPIDE)
var Personnage = {
    // Initialise le personnage
    init: function(nom, sante, force) {
        this.nom = nom;
        this.sante = sante;
        this.force = force;
        this.xp = 0;
    },
    
    // Renvoie la description du personnage
    decrire: function () {
        var description = this.nom + " a " + this.sante + " points de vie, " +
        this.force + " en force et " + this.xp + " points d'expérience";
        return description;
    }
};

// affectation des propriétés de perso1 en prenant comme prototype Personnage
var perso1 = Object.create(Personnage);
perso1.init("Aurora", 150, 25);

// affectation des propriétés de perso2 en prenant comme prototype Personnage
var perso2 = Object.create(Personnage);
perso2.init("Glacius", 130, 35);

console.log(perso1.decrire()); // decrire perso1
console.log(perso2.decrire()); // decrire perso2