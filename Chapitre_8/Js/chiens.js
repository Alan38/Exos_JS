// Creation de l'objet Chien
var Chien = {
    // fonction qui se charge d'initialiser un chien
    init: function(nom, race, taille) {
        this.nom = nom;
        this.race = race;
        this.taille = taille;
    },
    // fonction qui fait fait aboyer le chien concerné
    aboyer: function () {
        if (this.nom === "Crokdur") {
            var aboie = "Wouf ! Wouf !";
        }
        else {
            var aboie = "Kaii ! Kaii";
        }
        return aboie;
    },
};
// creation de l'objet crocdur avec ses propriétés
var crokdur = Object.create(Chien);
crokdur.init("Crokdur", "mâtin de Naples", 75);

// affichage
console.log(crokdur.nom + " est un " + crokdur.race + " mesurant " + crokdur.taille + " cm");
console.log("Tiens, un chat ! " + crokdur.nom + " aboie : " + crokdur.aboyer());

// creation de l'objet pupuce avec ses propriétés
var pupuce = Object.create(Chien);
pupuce.init("Pupuce", "bichon", 22);

// affichage
console.log(pupuce.nom + " est un " + pupuce.race + " mesurant " + pupuce.taille + " cm");
console.log("Tiens, un chat ! " + pupuce.nom + " aboie : " + pupuce.aboyer());