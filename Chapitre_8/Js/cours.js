// creation de l'objet unObjet
var unObjet = {
    a: 2 // affectaction de la propriété a qui vaut 2
};


// Crée unAutreObjet avec unObjet comme prototype
var unAutreObjet = Object.create(unObjet);

console.log(unAutreObjet.a); // affiche la propriété "a" de unAutreObjet
