// creation de l'objet Personnage
var Personnage = {
    // Initialise le personnage
    initPerso: function (nom, sante, force) {
        this.nom = nom;
        this.sante = sante
        this.force = force;
    },
    // Attaque un personnage cible
    attaquer: function (cible) {
        if (this.sante > 0) {
            var degats = this.force;
            console.log(this.nom + " attaque " + cible.nom + " et lui fait " + degats + " points de dégâts");
            cible.sante = cible.sante - degats;
            
            if (cible.sante > 0) {
            console.log(cible.nom + " a encore " + cible.sante + " points de vie");
            } 
            else {
                cible.sante = 0;
                console.log(cible.nom + " est mort !");
            }
        } 
        else {
            console.log(this.nom + " ne peut pas attaquer : il est mort...");
        }
    }
};

// creation de l'objet Joueur
var Joueur = Object.create(Personnage);
// fonction qui initialise le joueur
Joueur.initJoueur = function (nom, sante, force) {
    this.initPerso(nom, sante, force);
    this.xp = 0;
    this.piece = 10;
    this.cle = 1;
};
// fonction qui renvoie la description du joueur
Joueur.decrire = function () {
    var description = this.nom + " a " + this.sante + " points de vie, " + this.force + " en force, " + this.xp + " points d'expérience, " + this.piece + " pièces d'or et " + this.cle + " clé(s)";
    return description;
};
// fonction qui fait combattre contre un adversaire
Joueur.combattre = function (adversaire) {
    this.attaquer(adversaire);
    
    if (adversaire.sante === 0) {
        console.log(this.nom + " a tué " + adversaire.nom + " et gagne " +
        adversaire.valeur + " points d'expérience, ainsi que " + this.piece + " pièces d'or et " + this.cle + " clé(s)");
        this.xp += adversaire.valeur;
        this.piece += adversaire.piece;
        this.cle += adversaire.cle;
    }
};
//creation de l'objet Adversaire
var Adversaire = Object.create(Personnage);
// fonction qui initialise l'adversaire
Adversaire.initAdversaire = function (nom, sante, force, race, valeur) {
    this.initPerso(nom, sante, force);
    this.race = race;
    this.valeur = valeur;
    this.piece = 10;
    this.cle = 1;
};

// affectation des propriétés du joueur 1
var joueur1 = Object.create(Joueur);
joueur1.initJoueur("Aurora", 150, 25);

// affectation des propriétés du joueur 2
var joueur2 = Object.create(Joueur);
joueur2.initJoueur("Glacius", 130, 30);

// affichage des stats des joueurs
console.log("Bienvenue dans ce jeu d'aventure ! Voici nos courageux héros :");
console.log(joueur1.decrire());
console.log(joueur2.decrire());

// creation de l'objet monstre
var monstre = Object.create(Adversaire);
monstre.initAdversaire("ZogZog", 40, 20, "orc", 10);
// affichage du monstre
console.log("Un affreux monstre arrive : c'est un " + monstre.race + " nommé " + monstre.nom);

// le monstre utilise la fonction attaquer sur le j1 et j2
monstre.attaquer(joueur1);
monstre.attaquer(joueur2);

// le j1 et j2 combattent le monstre
joueur1.combattre(monstre);
joueur2.combattre(monstre);

// affichage du résultat de la bataille
console.log(joueur1.decrire());
console.log(joueur2.decrire());