// creation de l'objet CompteBancaire
var CompteBancaire = {
    // fonction d'initialisation du compte CB
    initCB: function (nom, solde) {
    this.nom = nom;
    this.solde = solde;
    },
    // fonction qui décrit le compte CB
    decrire: function () {
        var descriptionCB = "Nom du compte: " + this.nom + " | Solde: " + this.solde + "€";
        return descriptionCB;
    },
    // fonction qui débite le compte CB
    debiter: function (montant) {
        this.solde = this.solde - montant;
    },
    // fonction qui crédite le compte
    crediter: function (montant) {
        this.solde = this.solde + montant;
    }
};

var CompteEpargne = Object.create(CompteBancaire);
// Initialise le CompteEpargne
CompteEpargne.initCE = function (nom, solde, taux) {
    this.initCB(nom, solde);
    this.taux = taux;
};
//Calcule et ajoute les interets
CompteEpargne.ajouterInterets = function () {
    var interets = this.solde * this.taux;
    this.solde = this.solde + interets;
};

// creation du compte1 sur le modèle du CompteBancaire
var compte1 = Object.create(CompteBancaire);
compte1.initCB("Alex", 100);

// creation du compte2 sur le modèle du CompteEpargne
var compte2 = Object.create(CompteEpargne);
compte2.initCE("Marco", 50, 0.05);

// affiche l'était initial des comptes
console.log("Voici l'état initial des comptes :");
console.log(compte1.decrire());
console.log(compte2.decrire());

// demande a l'utilisateur le montant a transférer
var montant = Number(prompt("Entrez le montant à transférer entre les comptes :"));

// utilise les fonction débiter et créditer sur les 2 comptes pour faire le tranfert
compte1.debiter(montant);
compte2.crediter(montant);

// Calcule et ajoute les intérêts au solde du compte
compte2.ajouterInterets();

// affiche l'état final des comptes
console.log("Voici l'état final des comptes après transfert et calcul des intérêts :");
console.log(compte1.decrire());
console.log(compte2.decrire());
